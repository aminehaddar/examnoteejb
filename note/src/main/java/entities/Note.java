package entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "esp_note")
public class Note implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private NotePK notePk;
	private int note;
	@ManyToOne
	@JoinColumn(name = "idStudentPK", referencedColumnName = "idStudent", updatable = false, insertable = false)
	private Student student;
	@ManyToOne
	@JoinColumn(name = "idModulePK", referencedColumnName = "idModule", updatable = false, insertable = false)
	private Module module;

	public NotePK getNotePk() {
		return notePk;
	}

	public void setNotePk(NotePK notePk) {
		this.notePk = notePk;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public Note() {
		// TODO Auto-generated constructor stub
	}

}
