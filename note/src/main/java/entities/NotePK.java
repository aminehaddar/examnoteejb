package entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;

@Embeddable
public class NotePK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date date;
	private int idModulePK;
	private int idStudentPK;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getIdModulePK() {
		return idModulePK;
	}

	public void setIdModulePK(int idModulePK) {
		this.idModulePK = idModulePK;
	}

	public int getIdStudentPK() {
		return idStudentPK;
	}

	public void setIdStudentPK(int idStudentPK) {
		this.idStudentPK = idStudentPK;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + idModulePK;
		result = prime * result + idStudentPK;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotePK other = (NotePK) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (idModulePK != other.idModulePK)
			return false;
		if (idStudentPK != other.idStudentPK)
			return false;
		return true;
	}

	public NotePK() {
		// TODO Auto-generated constructor stub
	}

	public NotePK(Date date, int idModulePK, int idStudentPK) {
		super();
		this.date = date;
		this.idModulePK = idModulePK;
		this.idStudentPK = idStudentPK;
	}

}
