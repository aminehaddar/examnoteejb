package entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "esp_module")
@NamedQueries({
		@NamedQuery(name = "findModuleByName", query = "select m from Module m where m.name=:name "),
		@NamedQuery(name = "findModulesByClassRoom", query = "select distinct m from Module m join m.notes n where n.student.classRoom.name=:name")})
public class Module implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idModule;
	@Column(name = "name_module")
	private String name;
	private int duration;
	private String type;
	@OneToMany(mappedBy = "module")
	private List<Note> notes;

	public Module() {
		// TODO Auto-generated constructor stub
	}

	public Module(String name, int duration, String type) {
		super();
		this.name = name;
		this.duration = duration;
		this.type = type;
	}

	public int getIdModule() {
		return idModule;
	}

	public void setIdModule(int idModule) {
		this.idModule = idModule;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
