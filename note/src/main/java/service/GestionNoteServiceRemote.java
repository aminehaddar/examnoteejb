package service;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import entities.ClassRoom;
import entities.Module;
import entities.Student;

@Remote
public interface GestionNoteServiceRemote {

	boolean addModule(Module jee);

	boolean addClassRoom(ClassRoom erp1);

	ClassRoom findClassRoomById(int i);

	boolean affectStudentsToClassRoom(ClassRoom erp1, List<Student> students);

	boolean addnote(int i, Date date, String string, int j);

	List<Module> findAllModuleByClass(String string);

}
