package service;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import entities.ClassRoom;
import entities.Module;
import entities.Note;
import entities.NotePK;
import entities.Student;

@Stateless
public class GestionNoteService implements GestionNoteServiceRemote {

	@PersistenceContext
	EntityManager em;

	@Override
	public boolean addModule(Module jee) {
		try {
			em.persist(jee);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean addClassRoom(ClassRoom erp1) {
		try {
			em.persist(erp1);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public ClassRoom findClassRoomById(int i) {
		return em.find(ClassRoom.class, i);
	}

	@Override
	public boolean affectStudentsToClassRoom(ClassRoom erp1,
			List<Student> students) {
		try {
			for (Student student : students) {
				student.setClassRoom(erp1);
				em.merge(student);
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean addnote(int i, Date date, String string, int j) {
		try {
			NotePK pk = new NotePK(date,
					findModuleByName(string).getIdModule(), j);
			Note note = new Note();
			note.setNotePk(pk);
			note.setNote(i);
			em.persist(note);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private Module findModuleByName(String name) {
		TypedQuery<Module> query = em.createNamedQuery("findModuleByName",
				Module.class);
		query.setParameter("name", name);
		return query.getSingleResult();
	}

	@Override
	public List<Module> findAllModuleByClass(String string) {
			TypedQuery<Module> query = em.createNamedQuery("findModulesByClassRoom",
					Module.class);
			query.setParameter("name", string);
			return query.getResultList();
	}

}
